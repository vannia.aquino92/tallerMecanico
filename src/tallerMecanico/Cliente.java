package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CLIENTE_IDCLIENTE_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CLIENTE_IDCLIENTE_GENERATOR")
	@Column(name="id_cliente")
	private Integer idCliente;

	private String apellido;

	private String direccion;

	private String dni;

	private String email;

	private String nombre;

	private String telefono;

	//bi-directional many-to-one association to FacturaVentaCab
	@OneToMany(mappedBy="cliente")
	private Set<FacturaVentaCab> facturaventacabs;

	//bi-directional many-to-one association to Vehiculo
	@OneToMany(mappedBy="cliente")
	private Set<Vehiculo> vehiculos;

	public Cliente() {
	}

	public Integer getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Set<FacturaVentaCab> getFacturaventacabs() {
		return this.facturaventacabs;
	}

	public void setFacturaventacabs(Set<FacturaVentaCab> facturaventacabs) {
		this.facturaventacabs = facturaventacabs;
	}

	public FacturaVentaCab addFacturaventacab(FacturaVentaCab facturaventacab) {
		getFacturaventacabs().add(facturaventacab);
		facturaventacab.setCliente(this);

		return facturaventacab;
	}

	public FacturaVentaCab removeFacturaventacab(FacturaVentaCab facturaventacab) {
		getFacturaventacabs().remove(facturaventacab);
		facturaventacab.setCliente(null);

		return facturaventacab;
	}

	public Set<Vehiculo> getVehiculos() {
		return this.vehiculos;
	}

	public void setVehiculos(Set<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public Vehiculo addVehiculo(Vehiculo vehiculo) {
		getVehiculos().add(vehiculo);
		vehiculo.setCliente(this);

		return vehiculo;
	}

	public Vehiculo removeVehiculo(Vehiculo vehiculo) {
		getVehiculos().remove(vehiculo);
		vehiculo.setCliente(null);

		return vehiculo;
	}

}