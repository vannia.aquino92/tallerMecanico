package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the hojadepartecab database table.
 * 
 */
@Entity
@NamedQuery(name="HojaDeParteCab.findAll", query="SELECT h FROM HojaDeParteCab h")
public class HojaDeParteCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOJADEPARTECAB_IDHOJACAB_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOJADEPARTECAB_IDHOJACAB_GENERATOR")
	@Column(name="id_hojacab")
	private Integer idHojacab;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_creacion")
	private Date fechaCreacion;

	@Column(name="monto_total")
	private Integer montoTotal;

	//bi-directional many-to-one association to FacturaVentaDet
	@OneToMany(mappedBy="hojadepartecab")
	private Set<FacturaVentaDet> facturaventadets;

	//bi-directional many-to-one association to OrdenServicioCab
	@ManyToOne
	@JoinColumn(name="ordenserviciocab")
	private OrdenServicioCab ordenserviciocab1;

	//bi-directional many-to-one association to HojaDeParteDet
	@OneToMany(mappedBy="hojadepartecab")
	private Set<HojaDeParteDet> hojadepartedets;

	//bi-directional one-to-one association to OrdenServicioCab
	@OneToOne
	@JoinColumn(name="ordenserviciocab")
	private OrdenServicioCab ordenserviciocab2;

	public HojaDeParteCab() {
	}

	public Integer getIdHojacab() {
		return this.idHojacab;
	}

	public void setIdHojacab(Integer idHojacab) {
		this.idHojacab = idHojacab;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Integer getMontoTotal() {
		return this.montoTotal;
	}

	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Set<FacturaVentaDet> getFacturaventadets() {
		return this.facturaventadets;
	}

	public void setFacturaventadets(Set<FacturaVentaDet> facturaventadets) {
		this.facturaventadets = facturaventadets;
	}

	public FacturaVentaDet addFacturaventadet(FacturaVentaDet facturaventadet) {
		getFacturaventadets().add(facturaventadet);
		facturaventadet.setHojadepartecab(this);

		return facturaventadet;
	}

	public FacturaVentaDet removeFacturaventadet(FacturaVentaDet facturaventadet) {
		getFacturaventadets().remove(facturaventadet);
		facturaventadet.setHojadepartecab(null);

		return facturaventadet;
	}

	public OrdenServicioCab getOrdenserviciocab1() {
		return this.ordenserviciocab1;
	}

	public void setOrdenserviciocab1(OrdenServicioCab ordenserviciocab1) {
		this.ordenserviciocab1 = ordenserviciocab1;
	}

	public Set<HojaDeParteDet> getHojadepartedets() {
		return this.hojadepartedets;
	}

	public void setHojadepartedets(Set<HojaDeParteDet> hojadepartedets) {
		this.hojadepartedets = hojadepartedets;
	}

	public HojaDeParteDet addHojadepartedet(HojaDeParteDet hojadepartedet) {
		getHojadepartedets().add(hojadepartedet);
		hojadepartedet.setHojadepartecab(this);

		return hojadepartedet;
	}

	public HojaDeParteDet removeHojadepartedet(HojaDeParteDet hojadepartedet) {
		getHojadepartedets().remove(hojadepartedet);
		hojadepartedet.setHojadepartecab(null);

		return hojadepartedet;
	}

	public OrdenServicioCab getOrdenserviciocab2() {
		return this.ordenserviciocab2;
	}

	public void setOrdenserviciocab2(OrdenServicioCab ordenserviciocab2) {
		this.ordenserviciocab2 = ordenserviciocab2;
	}

}