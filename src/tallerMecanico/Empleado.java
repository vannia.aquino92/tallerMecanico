package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the empleado database table.
 * 
 */
@Entity
@NamedQuery(name="Empleado.findAll", query="SELECT e FROM Empleado e")
public class Empleado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EMPLEADO_IDEMPLEADO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EMPLEADO_IDEMPLEADO_GENERATOR")
	@Column(name="id_empleado")
	private Integer idEmpleado;

	private String apellido;

	private String email;

	private String nombre;

	private String telefono;

	//bi-directional many-to-one association to HojaDeParteDet
	@OneToMany(mappedBy="empleado")
	private Set<HojaDeParteDet> hojadepartedets;

	//bi-directional many-to-one association to OrdenServicioCab
	@OneToMany(mappedBy="empleado")
	private Set<OrdenServicioCab> ordenserviciocabs;

	public Empleado() {
	}

	public Integer getIdEmpleado() {
		return this.idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Set<HojaDeParteDet> getHojadepartedets() {
		return this.hojadepartedets;
	}

	public void setHojadepartedets(Set<HojaDeParteDet> hojadepartedets) {
		this.hojadepartedets = hojadepartedets;
	}

	public HojaDeParteDet addHojadepartedet(HojaDeParteDet hojadepartedet) {
		getHojadepartedets().add(hojadepartedet);
		hojadepartedet.setEmpleado(this);

		return hojadepartedet;
	}

	public HojaDeParteDet removeHojadepartedet(HojaDeParteDet hojadepartedet) {
		getHojadepartedets().remove(hojadepartedet);
		hojadepartedet.setEmpleado(null);

		return hojadepartedet;
	}

	public Set<OrdenServicioCab> getOrdenserviciocabs() {
		return this.ordenserviciocabs;
	}

	public void setOrdenserviciocabs(Set<OrdenServicioCab> ordenserviciocabs) {
		this.ordenserviciocabs = ordenserviciocabs;
	}

	public OrdenServicioCab addOrdenserviciocab(OrdenServicioCab ordenserviciocab) {
		getOrdenserviciocabs().add(ordenserviciocab);
		ordenserviciocab.setEmpleado(this);

		return ordenserviciocab;
	}

	public OrdenServicioCab removeOrdenserviciocab(OrdenServicioCab ordenserviciocab) {
		getOrdenserviciocabs().remove(ordenserviciocab);
		ordenserviciocab.setEmpleado(null);

		return ordenserviciocab;
	}

}