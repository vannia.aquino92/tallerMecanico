package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the facturaventacab database table.
 * 
 */
@Entity
@NamedQuery(name="FacturaVentaCab.findAll", query="SELECT f FROM FacturaVentaCab f")
public class FacturaVentaCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FACTURAVENTACAB_IDFACTURAVENTACAB_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FACTURAVENTACAB_IDFACTURAVENTACAB_GENERATOR")
	@Column(name="id_facturaventacab")
	private Integer idFacturaventacab;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name="total_monto")
	private Integer totalMonto;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	//bi-directional many-to-one association to FacturaVentaDet
	@OneToMany(mappedBy="facturaventacab")
	private Set<FacturaVentaDet> facturaventadets;

	public FacturaVentaCab() {
	}

	public Integer getIdFacturaventacab() {
		return this.idFacturaventacab;
	}

	public void setIdFacturaventacab(Integer idFacturaventacab) {
		this.idFacturaventacab = idFacturaventacab;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getTotalMonto() {
		return this.totalMonto;
	}

	public void setTotalMonto(Integer totalMonto) {
		this.totalMonto = totalMonto;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Set<FacturaVentaDet> getFacturaventadets() {
		return this.facturaventadets;
	}

	public void setFacturaventadets(Set<FacturaVentaDet> facturaventadets) {
		this.facturaventadets = facturaventadets;
	}

	public FacturaVentaDet addFacturaventadet(FacturaVentaDet facturaventadet) {
		getFacturaventadets().add(facturaventadet);
		facturaventadet.setFacturaventacab(this);

		return facturaventadet;
	}

	public FacturaVentaDet removeFacturaventadet(FacturaVentaDet facturaventadet) {
		getFacturaventadets().remove(facturaventadet);
		facturaventadet.setFacturaventacab(null);

		return facturaventadet;
	}

}