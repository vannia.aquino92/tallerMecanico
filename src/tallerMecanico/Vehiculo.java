package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the vehiculo database table.
 * 
 */
@Entity
@NamedQuery(name="Vehiculo.findAll", query="SELECT v FROM Vehiculo v")
public class Vehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VEHICULO_MATRICULA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VEHICULO_MATRICULA_GENERATOR")
	private String matricula;

	private String color;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_alta")
	private Date fechaAlta;

	private String modelo;

	//bi-directional many-to-one association to OrdenServicioCab
	@OneToMany(mappedBy="vehiculo")
	private Set<OrdenServicioCab> ordenserviciocabs;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="propietario")
	private Cliente cliente;

	public Vehiculo() {
	}

	public String getMatricula() {
		return this.matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Date getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Set<OrdenServicioCab> getOrdenserviciocabs() {
		return this.ordenserviciocabs;
	}

	public void setOrdenserviciocabs(Set<OrdenServicioCab> ordenserviciocabs) {
		this.ordenserviciocabs = ordenserviciocabs;
	}

	public OrdenServicioCab addOrdenserviciocab(OrdenServicioCab ordenserviciocab) {
		getOrdenserviciocabs().add(ordenserviciocab);
		ordenserviciocab.setVehiculo(this);

		return ordenserviciocab;
	}

	public OrdenServicioCab removeOrdenserviciocab(OrdenServicioCab ordenserviciocab) {
		getOrdenserviciocabs().remove(ordenserviciocab);
		ordenserviciocab.setVehiculo(null);

		return ordenserviciocab;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}