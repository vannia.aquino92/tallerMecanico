package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ordenserviciodet database table.
 * 
 */
@Entity
@NamedQuery(name="OrdenServicioDet.findAll", query="SELECT o FROM OrdenServicioDet o")
public class OrdenServicioDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ORDENSERVICIODET_IDORDENDET_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ORDENSERVICIODET_IDORDENDET_GENERATOR")
	@Column(name="id_ordendet")
	private Integer idOrdendet;

	@Column(name="precio_repuesto")
	private Integer precioRepuesto;

	@Column(name="precio_servicio")
	private Integer precioServicio;

	//bi-directional many-to-one association to OrdenServicioCab
	@ManyToOne
	@JoinColumn(name="ordenserviciocab")
	private OrdenServicioCab ordenserviciocabBean;

	//bi-directional many-to-one association to Repuesto
	@ManyToOne
	@JoinColumn(name="id_repuesto")
	private Repuesto repuesto;

	//bi-directional many-to-one association to Servicio
	@ManyToOne
	@JoinColumn(name="id_servicio")
	private Servicio servicio;

	public OrdenServicioDet() {
	}

	public Integer getIdOrdendet() {
		return this.idOrdendet;
	}

	public void setIdOrdendet(Integer idOrdendet) {
		this.idOrdendet = idOrdendet;
	}

	public Integer getPrecioRepuesto() {
		return this.precioRepuesto;
	}

	public void setPrecioRepuesto(Integer precioRepuesto) {
		this.precioRepuesto = precioRepuesto;
	}

	public Integer getPrecioServicio() {
		return this.precioServicio;
	}

	public void setPrecioServicio(Integer precioServicio) {
		this.precioServicio = precioServicio;
	}

	public OrdenServicioCab getOrdenserviciocabBean() {
		return this.ordenserviciocabBean;
	}

	public void setOrdenserviciocabBean(OrdenServicioCab ordenserviciocabBean) {
		this.ordenserviciocabBean = ordenserviciocabBean;
	}

	public Repuesto getRepuesto() {
		return this.repuesto;
	}

	public void setRepuesto(Repuesto repuesto) {
		this.repuesto = repuesto;
	}

	public Servicio getServicio() {
		return this.servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

}