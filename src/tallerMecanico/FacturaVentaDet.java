package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the facturaventadet database table.
 * 
 */
@Entity
@NamedQuery(name="FacturaVentaDet.findAll", query="SELECT f FROM FacturaVentaDet f")
public class FacturaVentaDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FACTURAVENTADET_IDFACTURAVENTADET_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FACTURAVENTADET_IDFACTURAVENTADET_GENERATOR")
	@Column(name="id_facturaventadet")
	private Integer idFacturaventadet;

	//bi-directional many-to-one association to FacturaVentaCab
	@ManyToOne
	@JoinColumn(name="id_facturaventacab")
	private FacturaVentaCab facturaventacab;

	//bi-directional many-to-one association to HojaDeParteCab
	@ManyToOne
	@JoinColumn(name="id_hojacab")
	private HojaDeParteCab hojadepartecab;

	public FacturaVentaDet() {
	}

	public Integer getIdFacturaventadet() {
		return this.idFacturaventadet;
	}

	public void setIdFacturaventadet(Integer idFacturaventadet) {
		this.idFacturaventadet = idFacturaventadet;
	}

	public FacturaVentaCab getFacturaventacab() {
		return this.facturaventacab;
	}

	public void setFacturaventacab(FacturaVentaCab facturaventacab) {
		this.facturaventacab = facturaventacab;
	}

	public HojaDeParteCab getHojadepartecab() {
		return this.hojadepartecab;
	}

	public void setHojadepartecab(HojaDeParteCab hojadepartecab) {
		this.hojadepartecab = hojadepartecab;
	}

}