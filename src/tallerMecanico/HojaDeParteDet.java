package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the hojadepartedet database table.
 * 
 */
@Entity
@NamedQuery(name="HojaDeParteDet.findAll", query="SELECT h FROM HojaDeParteDet h")
public class HojaDeParteDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HOJADEPARTEDET_IDHOJADET_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOJADEPARTEDET_IDHOJADET_GENERATOR")
	@Column(name="id_hojadet")
	private Integer idHojadet;

	@Column(name="mano_obra")
	private Integer manoObra;

	//bi-directional many-to-one association to Empleado
	@ManyToOne
	@JoinColumn(name="mecanico")
	private Empleado empleado;

	//bi-directional many-to-one association to HojaDeParteCab
	@ManyToOne
	@JoinColumn(name="id_hojacab")
	private HojaDeParteCab hojadepartecab;

	//bi-directional many-to-one association to Repuesto
	@ManyToOne
	@JoinColumn(name="id_repuesto")
	private Repuesto repuesto;

	public HojaDeParteDet() {
	}

	public Integer getIdHojadet() {
		return this.idHojadet;
	}

	public void setIdHojadet(Integer idHojadet) {
		this.idHojadet = idHojadet;
	}

	public Integer getManoObra() {
		return this.manoObra;
	}

	public void setManoObra(Integer manoObra) {
		this.manoObra = manoObra;
	}

	public Empleado getEmpleado() {
		return this.empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public HojaDeParteCab getHojadepartecab() {
		return this.hojadepartecab;
	}

	public void setHojadepartecab(HojaDeParteCab hojadepartecab) {
		this.hojadepartecab = hojadepartecab;
	}

	public Repuesto getRepuesto() {
		return this.repuesto;
	}

	public void setRepuesto(Repuesto repuesto) {
		this.repuesto = repuesto;
	}

}