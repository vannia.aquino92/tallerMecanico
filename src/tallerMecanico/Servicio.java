package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the servicio database table.
 * 
 */
@Entity
@NamedQuery(name="Servicio.findAll", query="SELECT s FROM Servicio s")
public class Servicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICIO_IDSERVICIO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICIO_IDSERVICIO_GENERATOR")
	@Column(name="id_servicio")
	private Integer idServicio;

	private String nombre;

	//bi-directional many-to-one association to OrdenServicioDet
	@OneToMany(mappedBy="servicio")
	private Set<OrdenServicioDet> ordenserviciodets;

	public Servicio() {
	}

	public Integer getIdServicio() {
		return this.idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<OrdenServicioDet> getOrdenserviciodets() {
		return this.ordenserviciodets;
	}

	public void setOrdenserviciodets(Set<OrdenServicioDet> ordenserviciodets) {
		this.ordenserviciodets = ordenserviciodets;
	}

	public OrdenServicioDet addOrdenserviciodet(OrdenServicioDet ordenserviciodet) {
		getOrdenserviciodets().add(ordenserviciodet);
		ordenserviciodet.setServicio(this);

		return ordenserviciodet;
	}

	public OrdenServicioDet removeOrdenserviciodet(OrdenServicioDet ordenserviciodet) {
		getOrdenserviciodets().remove(ordenserviciodet);
		ordenserviciodet.setServicio(null);

		return ordenserviciodet;
	}

}