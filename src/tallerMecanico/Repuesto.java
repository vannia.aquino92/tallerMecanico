package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the repuesto database table.
 * 
 */
@Entity
@NamedQuery(name="Repuesto.findAll", query="SELECT r FROM Repuesto r")
public class Repuesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="REPUESTO_IDREPUESTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPUESTO_IDREPUESTO_GENERATOR")
	@Column(name="id_repuesto")
	private Integer idRepuesto;

	private String nombre;

	//bi-directional many-to-one association to HojaDeParteDet
	@OneToMany(mappedBy="repuesto")
	private Set<HojaDeParteDet> hojadepartedets;

	//bi-directional many-to-one association to OrdenServicioDet
	@OneToMany(mappedBy="repuesto")
	private Set<OrdenServicioDet> ordenserviciodets;

	public Repuesto() {
	}

	public Integer getIdRepuesto() {
		return this.idRepuesto;
	}

	public void setIdRepuesto(Integer idRepuesto) {
		this.idRepuesto = idRepuesto;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<HojaDeParteDet> getHojadepartedets() {
		return this.hojadepartedets;
	}

	public void setHojadepartedets(Set<HojaDeParteDet> hojadepartedets) {
		this.hojadepartedets = hojadepartedets;
	}

	public HojaDeParteDet addHojadepartedet(HojaDeParteDet hojadepartedet) {
		getHojadepartedets().add(hojadepartedet);
		hojadepartedet.setRepuesto(this);

		return hojadepartedet;
	}

	public HojaDeParteDet removeHojadepartedet(HojaDeParteDet hojadepartedet) {
		getHojadepartedets().remove(hojadepartedet);
		hojadepartedet.setRepuesto(null);

		return hojadepartedet;
	}

	public Set<OrdenServicioDet> getOrdenserviciodets() {
		return this.ordenserviciodets;
	}

	public void setOrdenserviciodets(Set<OrdenServicioDet> ordenserviciodets) {
		this.ordenserviciodets = ordenserviciodets;
	}

	public OrdenServicioDet addOrdenserviciodet(OrdenServicioDet ordenserviciodet) {
		getOrdenserviciodets().add(ordenserviciodet);
		ordenserviciodet.setRepuesto(this);

		return ordenserviciodet;
	}

	public OrdenServicioDet removeOrdenserviciodet(OrdenServicioDet ordenserviciodet) {
		getOrdenserviciodets().remove(ordenserviciodet);
		ordenserviciodet.setRepuesto(null);

		return ordenserviciodet;
	}

}