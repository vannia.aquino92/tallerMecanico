package tallerMecanico;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the ordenserviciocab database table.
 * 
 */
@Entity
@NamedQuery(name="OrdenServicioCab.findAll", query="SELECT o FROM OrdenServicioCab o")
public class OrdenServicioCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ORDENSERVICIOCAB_ORDENSERVICIOCAB_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ORDENSERVICIOCAB_ORDENSERVICIOCAB_GENERATOR")
	private Integer ordenserviciocab;

	private String estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_creacion")
	private Date fechaCreacion;

	@Column(name="monto_total")
	private Integer montoTotal;

	//bi-directional many-to-one association to HojaDeParteCab
	@OneToMany(mappedBy="ordenserviciocab1")
	private Set<HojaDeParteCab> hojadepartecabs;

	//bi-directional many-to-one association to Empleado
	@ManyToOne
	@JoinColumn(name="mecanico")
	private Empleado empleado;

	//bi-directional many-to-one association to Vehiculo
	@ManyToOne
	@JoinColumn(name="id_vehiculo")
	private Vehiculo vehiculo;

	//bi-directional many-to-one association to OrdenServicioDet
	@OneToMany(mappedBy="ordenserviciocabBean")
	private Set<OrdenServicioDet> ordenserviciodets;

	//bi-directional one-to-one association to HojaDeParteCab
	@OneToOne(mappedBy="ordenserviciocab2")
	private HojaDeParteCab hojadepartecab;

	public OrdenServicioCab() {
	}

	public Integer getOrdenserviciocab() {
		return this.ordenserviciocab;
	}

	public void setOrdenserviciocab(Integer ordenserviciocab) {
		this.ordenserviciocab = ordenserviciocab;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Integer getMontoTotal() {
		return this.montoTotal;
	}

	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Set<HojaDeParteCab> getHojadepartecabs() {
		return this.hojadepartecabs;
	}

	public void setHojadepartecabs(Set<HojaDeParteCab> hojadepartecabs) {
		this.hojadepartecabs = hojadepartecabs;
	}

	public HojaDeParteCab addHojadepartecab(HojaDeParteCab hojadepartecab) {
		getHojadepartecabs().add(hojadepartecab);
		hojadepartecab.setOrdenserviciocab1(this);

		return hojadepartecab;
	}

	public HojaDeParteCab removeHojadepartecab(HojaDeParteCab hojadepartecab) {
		getHojadepartecabs().remove(hojadepartecab);
		hojadepartecab.setOrdenserviciocab1(null);

		return hojadepartecab;
	}

	public Empleado getEmpleado() {
		return this.empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Set<OrdenServicioDet> getOrdenserviciodets() {
		return this.ordenserviciodets;
	}

	public void setOrdenserviciodets(Set<OrdenServicioDet> ordenserviciodets) {
		this.ordenserviciodets = ordenserviciodets;
	}

	public OrdenServicioDet addOrdenserviciodet(OrdenServicioDet ordenserviciodet) {
		getOrdenserviciodets().add(ordenserviciodet);
		ordenserviciodet.setOrdenserviciocabBean(this);

		return ordenserviciodet;
	}

	public OrdenServicioDet removeOrdenserviciodet(OrdenServicioDet ordenserviciodet) {
		getOrdenserviciodets().remove(ordenserviciodet);
		ordenserviciodet.setOrdenserviciocabBean(null);

		return ordenserviciodet;
	}

	public HojaDeParteCab getHojadepartecab() {
		return this.hojadepartecab;
	}

	public void setHojadepartecab(HojaDeParteCab hojadepartecab) {
		this.hojadepartecab = hojadepartecab;
	}

}